var expect  = require('chai').expect;
var request = require('request');
var fs = require('fs');

var requestPromise = require('request-promise');


describe("mockup", function() {


     var newPatientIanMcKellenID;




    it("mockup Tests Post Request create New Patient", function(done) {



        var newPatientIanMcKellen = { 

        	"first_name":"Ian",
       		"last_name":"McKellen",
			"date_of_birth":"05/25/1939", 
			"sex" : "M",
			"height" : 180,
			"weight" : 69,
			"address":"75 Sundance Crescent",
			"city" : "Scarborough",
			"zip_code" : "M1G 2M1",
			"emergency_contact" : "Brian Taylor",
			"emergency_contact_relationship" : "Partner",
			"emergency_contact_number" : "416 123 0109",
			"email" : "sir.mckellen@gmail.com",
			"room" : "A101",
			"note" : "Not allergic",
            "condition" : "GOOD"
        };




        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/patients/',
            body: newPatientIanMcKellen,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("IanMcKellen was created");
            newPatientIanMcKellenID = response.body._id;
            console.log(testPatientID);
            expect(response.statusCode).to.equal(200);
            done();



        })
        .catch(function (err) {
            done();

        });


        

    });



    it("mockup picture profiles", function(done) {


  
        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/pictures/' + newPatientIanMcKellenID,

            formData: {
                picture: {
                        value: fs.createReadStream('./test/ian.png'),
                        options: {
                            filename: 'picture',
                            contentType: 'image/png'
                        }
                }

            },
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("IanMcKellen profile pic created");
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    

    });

    



    


    var newNurseAbbyID;

    it("mockup Tests Post Request create New Nurse", function(done) {



        var newNurseAbby = {    
        	"first_name":"Abby", 
			"last_name":"Lockhart",
			"register_nurse_id": "RN100123123",
			"shift" : "Morning",
			"admin" : false,
			"password" : "123",
			"date_of_birth":"02/03/1965", 
			"address":"75 Sundance Crescent",
			"city" : "Scarborough",
			"zip_code" : "M1G 2M1",
			"number" : "416 123 0109",
			"email" : "abby.lockhart@gmail.com"
            
        };



        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/nurses',
            body: newNurseAbby,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("New nurse Abby was created");
            newNurseAbbyID = response.body._id
            expect(response.statusCode).to.equal(200);
            done()


        })
        .catch(function (err) {
           console.log("Error New nurse Abby was created");

            done();

        });

    });


    it("mockup picture profile Nurse", function(done) {

  
        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/pictures/' + newNurseAbbyID,

            formData: {
                picture: {
                        value: fs.createReadStream('./test/nurse.png'),
                        options: {
                            filename: 'picture',
                            contentType: 'image/png'
                        }
                }

            },
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            expect(response.statusCode).to.equal(200);
            done();
            console.log("Profile pic nurse Abby was created");


        })
        .catch(function (err) {
            done();

        });

    });



 	var newTaskCheckupID;

    it("mockup Tests Post Request create New Task", function(done) {



        var newTaskCheckup = {     
        			"patient_id":newPatientIanMcKellenID, 
 					"nurse_id":newNurseAbbyID,
 					"timestamp":"1573608018",
 					"duration":"2 hours",
 					 "category":"Checkup",
 					 "time":"8 am",
 					 "date":"Mon Dec 09 2019",
 					 "note":"Full checkup",
 					 "completed":false,
 					 "title":"Weekly Checkup"
        };

  


        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/tasks/',
            body: newTaskCheckup,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("New task checkup created");
            newTaskCheckupID = response.body._id
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });


    var newTaskFoodID;

    it("mockup Tests Post Request create New Task", function(done) {



        var newTaskFood = {     
                    "patient_id":newPatientIanMcKellenID, 
                     "nurse_id":newNurseAbbyID,
                     "timestamp":"1573608018",
                     "duration":"2 hours",
                      "category":"Food",
                      "time":"8 am",
                      "date":"Mon Dec 09 2019",
                      "note":"Give patient lunch",
                      "completed":false,
                      "title":"Dayly lunch"
        };

  


        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/tasks/',
            body: newTaskFood,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("New task lunch created");
            newTaskFoodID = response.body._id
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });


    it("mockup Tests Post Request create New Report", function(done) {



        var newReportCheckup = {     
        			"patient_id":newPatientIanMcKellenID, 
 					"nurse_id":newNurseAbbyID,
 					"task_id" : newTaskCheckupID,
 					"timestamp":"1573608018",
 					"note":"This is a checkup report created for test",
 					"readings":[{"name":"Blood Pressure ", "result":"200/110 mm Hg" },{"name":"Pulse Rate", "result":"40 beats per minute" },{"name":"Body temperature ", "result":"37 degrees Celsius" },{"name":"Blood Oxygen Level", "result" :"60 mm Hg "},{"name":"Heart Beat Rate ", "result":"100 bpm "},{"name":"Respiratory Rate ", "result":"26 breaths/minute" },{"name":"hearing ", "result":"normal"}, {"name":"diabetes ", "result":"100 mg/dL" },{"name":"bone density", "result" :"0.9 "},{"name":"Thyroid  ", "result":"4.0 mU/L"}, {"name":"height " , "result":"175.4 cm"},{"name":"weight " , "result":"68.6 kg" },{"name":"Bilirubin " , "result" :"0.3 mg/dL"}, {"name":"Blood group ", "result":"O+ve "},{"name":"Hemoglobin " , "result":"3.5 to 17.5 gm/dL"}, {"name":"Hdl ", "result":"40 mg/dl" },{"name":"ldl ", "result":"143 mg/dl "},{"name":"blood count ", "result":"17.5 gm/dL "},{"name":"Urinalysis", "result" :"4.5" },{"name":"Hearing capacity ", "result":"41 to 55 decibels "}]
            
        };




        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/reports/',
            body: newReportCheckup,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log(response.body._id);
            expect(response.statusCode).to.equal(200);
            done();
            


        })
        .catch(function (err) {
            done();

        });

    });


    it("mockup Tests Post Request create New Report", function(done) {



        var newReportFood = {     
                    "patient_id":newPatientIanMcKellenID, 
                     "nurse_id":newNurseAbbyID,
                     "task_id" : newTaskFoodID,
                     "timestamp":"1573608018",
                     "note":"This is a checkup report created for test",
                     "readings":[{"name":"Where did the resident have breakfast?", "result":"Dining Room" },{"name":"How much did the resident eat?", "result":"75%"}]
            
        };




        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/reports/',
            body: newReportFood,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log(response.body._id);
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });
	


 });