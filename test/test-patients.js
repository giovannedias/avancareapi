var expect  = require('chai').expect;
var request = require('request');

var requestPromise = require('request-promise');



 describe("Test Patients from API", function() {

    it("Tests Request all patients", function(done) {
            var options = {
                            method: 'GET',
                            uri: 'http://localhost:3000/patients',
                            json: true, // Automatically stringifies the body to JSON
                            resolveWithFullResponse: true
            };

            requestPromise(options)
                .then(function (response,body) {
                        expect(response.statusCode).to.equal(200);
                        done();
                })
                .catch(function (err) {
                        done();

            });


    });

    var testPatientID;

    it("Tests Post Request create New Patient", function(done) {



        var newPatient = {     "first_name":"New Patient", 
                               "last_name":"Mocha Test",
                               "address":"941 Progress Av.",
                               "date_of_birth":"10/10/1985",
                               "department":"Emergency", 
                               "doctor":"John Smith"
            
        };



        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/patients/',
            body: newPatient,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("New patient created");
            testPatientID = response.body._id
            console.log(testPatientID);
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });


    it("Tests Get Request Patient by ID", function(done) {


        var options = {
            method: 'GET',
            uri: 'http://localhost:3000/patients/' + testPatientID,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };



        requestPromise(options)
        .then(function (response,body) {
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });

     it("Tests update Patient by ID", function(done) {


         var updatedPatient = {    "first_name":"New Patient Updated", 
                                   "last_name":"Mocha Test",
                                   "address":"941 Progress Av.",
                                   "date_of_birth":"10/10/1985",
                                   "department":"Emergency", 
                                   "doctor":"John Smith"
            
        };



        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/patients/'+ testPatientID,
            body: updatedPatient,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };


   


        requestPromise(options)
        .then(function (response,body) {
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });


     it("Tests Delete Patient by ID", function(done) {


        var options = {
            method: 'DELETE',
            uri: 'http://localhost:3000/patients/' + testPatientID,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };



        requestPromise(options)
        .then(function (response,body) {
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });
 

 });



