var expect  = require('chai').expect;
var request = require('request');
var fs = require('fs');

var requestPromise = require('request-promise');


describe("mockup", function() {

it("mockup picture profile Patient", function(done) {

   	   var testPatientID = "5ded8ec497cb0e795a7b82d1";
  
        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/pictures/' + testPatientID,

            formData: {
                picture: {
                        value: fs.createReadStream('./test/ian.png'),
                        options: {
                            filename: 'picture',
                            contentType: 'image/png'
                        }
                }

            },
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("IanMcKellen profile pic created");
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });





});