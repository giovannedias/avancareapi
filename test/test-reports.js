var expect  = require('chai').expect;
var request = require('request');

var requestPromise = require('request-promise');




 describe("Test Reports from API", function() {



 	var testPatientID;

    it("Tests Post Request create New Patient", function(done) {



        var newPatient = {     "first_name":"New Patient", 
                               "last_name":"Mocha Test",
                               "address":"941 Progress Av.",
                               "date_of_birth":"10/10/1985",
                               "department":"Emergency", 
                               "doctor":"John Smith"
            
        };



        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/patients/',
            body: newPatient,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("New patient created");
            testPatientID = response.body._id
            console.log(testPatientID);
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });


    var testNurseID;

    it("Tests Post Request create New Nurse", function(done) {



        var newNurse = {     "first_name":"New Nurse", 
                               "last_name":"Mocha Test",
                               "address":"941 Progress Av.",
                               "date_of_birth":"10/10/1985",
                               "registered_nurse_identification":"RN0001000234", 
                               "password":"test",
                               "admin":false
            
        };



        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/nurses',
            body: newNurse,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("New nurse created");
            testNurseID = response.body._id
            console.log(testNurseID);
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });


    it("Tests Post Request create New Report", function(done) {



        var newReport = {     
        			"patient_id":testPatientID, 
 					"nurse_id":testNurseID,
 					"timestamp":"1573608018",
 					"type":"Mocha test 2", 
 					"category":"Blood Pressure",
 					"note":"this is a report created for test",
 					"readings":[{"name": "diastolic", "result":70}, 
								{"name": "systolic", "result":130}]
            
        };



        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/patients/'+ testPatientID +'/reports',
            body: newReport,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };





        requestPromise(options)
        .then(function (response,body) {
            console.log("New report created");
            console.log(response.body._id);
            expect(response.statusCode).to.equal(200);
            done();


        })
        .catch(function (err) {
            done();

        });

    });
	


 });


