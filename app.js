const Express = require("express");
const Mongoose = require("mongoose");
const BodyParser = require("body-parser");
const multer = require('multer');
var fs = require('fs');

var request = require('request');


var app = Express();
var Schema = Mongoose.Schema

app.use(BodyParser.json({limit: '50mb'}));
app.use(BodyParser.urlencoded({limit: '50mb', extended: true }));


app.use("/pictures", Express.static(__dirname + '/pictures'));



app.listen(3000, () => {
    console.log("Listening at :3000...");
});


//m2
Mongoose.connect("mongodb://localhost/avencareMongo05");



const PatientModel = Mongoose.model("patient", {
    first_name: String,
    last_name: String,
    condition: String,
    date_of_birth: String,
    sex: String,
    height: Number,
    weight: Number,
    address: String,
    city: String,
    zip_code: String,
    emergency_contact: String,
    emergency_contact_relationship: String,
    emergency_contact_number:String,
    email: String,
    room: String,
    note: String,
    reports: [{ type: Schema.Types.ObjectId, ref: 'ReportModel'}]

});

const NurseModel = Mongoose.model("nurse", {
    first_name: String,
    last_name: String,
    register_nurse_id: String,
    shift: String,
    admin: Boolean,
    password: String,
    date_of_birth: String,
    address: String,
    city: String,
    zip_code: String,
    number: String,
    email: String,
    status: String,
    reports: [{ type: Schema.Types.ObjectId, ref: 'ReportModel'}]


});



const ReportModel = Mongoose.model("report", {
    timestamp: String,
    note: String,
    nurse_id: String,
    patient_id: String,
    task_id: String,
    nurse: {
        first_name: String,
        last_name: String,
        register_nurse_id: String,
        shift: String,
        admin: Boolean
    },
    patient:  {
        first_name: String,
        last_name: String,
        condition: String,
        sex: String,
        height: Number,
        weight: Number,
        emergency_contact: String,
        emergency_contact_relationship: String,
        emergency_contact_number:String,
        room: String,
        note: String,
    },
    task: { 
        timestamp: String,
        duration: String,
        category: String,
        time_to_start: String,
        note: String,
        completed: Boolean,
        title: String
    },
    readings: [{
         name: String,
         result: String
    }]
});


const TaskModel = Mongoose.model("task", {
    patient_id: String,
    patient_name: String,
    nurse_name: String,
    nurse_id: String,
    timestamp: String,
    category: String,
    time: String,
    date: String,
    duration: String,
    note: String,
    completed: Boolean,
    title: String,
    patient: { type: Schema.Types.ObjectId, ref: 'ReportModel'},
    nurse: { type: Schema.Types.ObjectId, ref: 'NurseModel'},

});

const AlarmModel = Mongoose.model("alarm", {
    nurse_id: String,
    allNurses: Boolean,
    timestamp: String,
    message: String,
    completed: Boolean,
    title: String

});


// SET STORAGE
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'pictures')
  },
  filename: function (req, file, cb) {
    cb(null, req.params.id + '.png')
  }
})
 
var upload = multer({ storage: storage })



var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
   res.header("Access-Control-Allow-Headers", "Origin, Access-Control-Allow-Headers, X-Requested-With, Content-Type, Accept, Authorization");

  next();
 });


app.get("/helloworld", async (request, response) => {
    try {

        console.log("request received");
        response.send('Hello World');
    } catch (error) {
        response.status(500).send(error);
    }
});


/*Create new alarm */

app.post("/alarm", async (request, response) => {


    try {
        var alarm = new AlarmModel(request.body);
        var result = await alarm.save();
        console.log("New Alarm");
        console.log(result)
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*Delete new alarm */

app.delete("/alarms/:id", async (request, response) => {
    try {
        var result = await AlarmModel.deleteOne({ _id: request.params.id }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*Update alarm by id */


app.post("/alarms/:id", async (request, response) => {
    try {

        var result = await AlarmModel.findOneAndUpdate({ _id: request.params.id },request.body ).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*get all alarms*/


app.get("/alarms", async (request, response) => {
    try {

        console.log("Get all alarms");

        var alarms = await AlarmModel.find().exec();
        
        response.send(alarms);
    } catch (error) {
        response.status(500).send(error);
    }
});




/*Delete task */
app.delete("/tasks/:id", async (request, response) => {
    try {
        var result = await TaskModel.deleteOne({ _id: request.params.id }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});
/*Create new task */

app.post("/tasks", async (request, response) => {
        console.log("New task");
        

    try {
        var task = new TaskModel(request.body);


        var patient = await PatientModel.findById(request.body.patient_id).exec();
        var nurse = await NurseModel.findById(request.body.nurse_id).exec();
        task.patient = patient;
        task.nurse = nurse;

        var result = await task.save();
        console.log(result)
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*Search tasks by name */


app.get("/tasks/search/", async (request, response) => {
    try {

//db.bios.find( { "name.last": "Hopper" } )
        console.log("search task:")
        let search = request.query.search;

        console.log(search)
        var result = await TaskModel.find({
            $or: [
                {'patient.first_name': {$regex:  new RegExp(search, "i")}},
                {'patient.last_name': {$regex:  new RegExp(search, "i")}},
                {'nurse.first_name': {$regex:  new RegExp(search, "i")}},
                {'nurse.last_name': {$regex:  new RegExp(search, "i")}},
                {'title': {$regex:  new RegExp(search, "i")}},
                {'note': {$regex:  new RegExp(search, "i")}},
                {'category': {$regex:  new RegExp(search, "i")}},
                {'patient.room': {$regex:  new RegExp(search, "i")}}
            ]
        })
        .populate('patient', 'patient_id first_name last_name condition',PatientModel)
        .populate('nurse', 'nurse_id first_name last_name',NurseModel)
        .exec();
        //.populate('reports', 'nurse_id timestamp type category',ReportModel).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});


/*All tasks */
app.get("/tasks", async (request, response) => {
    try {

        console.log("Get all tasks");

        var tasks = await TaskModel.find()
        .populate('patient', 'patient_id first_name last_name condition',PatientModel)
        .populate('nurse', 'nurse_id first_name last_name',NurseModel)
        .exec();
        //console.log(tasks);
        //.populate('reports', 'nurse_id timestamp type category',ReportModel)
      
        response.send(tasks);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*Get task by id */


app.get("/tasks/:id", async (request, response) => {
    try {

        console.log("/tasks/:id")

        var task = await TaskModel.findById(request.params.id).exec();
        //.populate('reports', 'nurse_id timestamp type category',ReportModel).exec();
        response.send(task);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*Update task by id */


app.post("/tasks/:id", async (request, response) => {
    try {

        var result = await TaskModel.findOneAndUpdate({ _id: request.params.id },request.body ).exec();

        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});



/*Search patients by name */


app.get("/patients/search/", async (request, response) => {
    try {

//db.bios.find( { "name.last": "Hopper" } )
        console.log("Query:")
        let search = request.query.search;
        let searchLowerCase = search.toLowerCase();

        console.log(search)
        var result = await PatientModel.find({
            $or: [
                {"last_name": { $regex:  new RegExp(search, "i") }},
                {"first_name": { $regex:  new RegExp(search, "i") }}
            ]
        }).exec();
        //.populate('reports', 'nurse_id timestamp type category',ReportModel).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*All patients */
app.get("/patients", async (request, response) => {
    try {
        var result = await PatientModel.find().exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});



/*Create new patient */

app.post("/patients", async (request, response) => {


    try {
        var patient = new PatientModel(request.body);
        var result = await patient.save();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*Get patient by id */


app.get("/patients/:id", async (request, response) => {
    try {

        console.log("/patients/:id")

        var patient = await PatientModel.findById(request.params.id).exec();
        //.populate('reports', 'nurse_id timestamp type category',ReportModel).exec();
        response.send(patient);
    } catch (error) {
        response.status(500).send(error);
    }
});


/*Update patient by id */


app.post("/patients/:id", async (request, response) => {
    try {

        var result = await PatientModel.findOneAndUpdate({ _id: request.params.id },request.body ).exec();

        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});






/*Delete patient by id */


app.delete("/patients/:id", async (request, response) => {
    try {
        var result = await PatientModel.deleteOne({ _id: request.params.id }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});


/*Search nurses by name */


app.get("/nurses/search/", async (request, response) => {
    try {

        console.log("/nurses/search/");

        let search = request.query.search;

        var result = await NurseModel.find({
            $or: [
                {"last_name": { $regex:  new RegExp(search, "i") }},
                {"first_name": { $regex:  new RegExp(search, "i") }}
            ]
        }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*All nurses */

app.get("/nurses", async (request, response) => {
    try {
        var result = await NurseModel.find().exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

/*Create new nurse */


app.post("/nurses", async (request, response) => {

    try {
        var nurse = new NurseModel(request.body);
        var result = await nurse.save();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});


/*Update nurse by id */


app.post("/nurses/:id", async (request, response) => {
    try {

        var result = await NurseModel.findOneAndUpdate({ _id: request.params.id },request.body ).exec();

        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});






/*Get nurse by id */

app.get("/nurses/:id", async (request, response) => {
    try {
        var nurse = await NurseModel.findById(request.params.id).populate('reports', 'pacient_id timestamp type category readings',ReportModel).exec();
        response.send(nurse);
    } catch (error) {
        response.status(500).send(error);
    }
});


/*Delete nurse by id */


app.delete("/nurses/:id", async (request, response) => {
    try {
        var result = await NurseModel.deleteOne({ _id: request.params.id }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});


/*Get reports by search */

app.get("/reports/search/", async (request, response) => {
    try {

        console.log("/reports/search/");

        let search = request.query.search;

        console.log(search);


        var result = await ReportModel.find({
            $or: [
                {note: { $regex:  new RegExp(search, "i") }},
                {'patient.first_name': {$regex:  new RegExp(search, "i")}},
                {'patient.last_name': {$regex:  new RegExp(search, "i")}},
                {'nurse.first_name': {$regex:  new RegExp(search, "i")}},
                {'nurse.last_name': {$regex:  new RegExp(search, "i")}},
                {'task.title': {$regex:  new RegExp(search, "i")}},
                {'task.note': {$regex:  new RegExp(search, "i")}},
                {'task.category': {$regex:  new RegExp(search, "i")}},
                {'patient.room': {$regex:  new RegExp(search, "i")}}


            ]
        }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});





/*Get all reports */

app.get("/reports", async (request, response) => {

    try {

        var result = await ReportModel.find().exec();
        response.send(result);

    } catch (error) {
        response.status(500).send(error);
    }
});

/*Get report by id */

app.get("/reports/:id", async (request, response) => {

    try {

        var report = await ReportModel.findById(request.params.id).exec();
        response.send(report.populate('nurse',NurseModel));

    } catch (error) {
        response.status(500).send(error);
    }
});


/*Update report by id */


app.post("/reports/:id", async (request, response) => {
    try {

        var result = await ReportModel.findOneAndUpdate({ _id: request.params.id },request.body ).exec();

        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});



/*Delete report by id */


app.delete("/reports/:id", async (request, response) => {
    try {
        var result = await ReportModel.deleteOne({ _id: request.params.id }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});



/*Create new report */


app.post("/reports/", async (request, response) => {

    try {

        console.log("new /reports");

       
        var report = new ReportModel(request.body);

        var patient = await PatientModel.findById(report.patient_id).exec();
        var nurse = await NurseModel.findById(report.nurse_id).exec();
        var task = await TaskModel.findById(report.task_id).exec();

        /*if (task.completed == false) {

            response.status(406).send("Task must be completed to create a report.");
            return;

        }*/

    
        report.nurse = nurse;
        report.patient = patient;
        report.task = task;

        console.log(report);

        var result = await report.save();


       response.send(result);
        
     

    } catch (error) {

        console.log("Error");
        console.log(error);


        response.status(500).send(error);
    }
});


/*Create new report for patient with nurse ID */


app.post("/patients/:id/reports", async (request, response) => {

    try {

        console.log("/patients/:id/reports");

       
        var report = new ReportModel(request.body);

        console.log(request.body);


        var patient = await PatientModel.findById(request.params.id).exec();
        var nurse_id = report.nurse_id;
        var nurse = await NurseModel.findById(nurse_id).exec();


        
        patient.reports.push(report);
        nurse.reports.push(report);

        var result = await report.save();
        result = await patient.save();

        result = await nurse.save();
        result = await report.save();

        response.send(report.populate('readings', 'name result',ReportModel));

    } catch (error) {

        console.log("Error");
        console.log(error);


        response.status(500).send(error);
    }
});

/*Get reports from patient with ID */

app.get("/patients/:id/reports", async (request, response) => {

    try {

        var reports = await ReportModel.find({ patient_id: request.params.id});
        response.send(reports);

    } catch (error) {
        response.status(500).send(error);
    }
});




/*Get task by nurse */
app.get("/nurses/:id/tasks", async (request, response) => {
    try {

        console.log("Get all tasks");
        console.log(request.params.id);


        var tasks = await TaskModel.find({ nurse_id: request.params.id}).populate('patient', 'patient_id first_name last_name condition room',PatientModel)
        .populate('nurse', 'nurse_id first_name last_name',NurseModel);


        response.send(tasks);
    } catch (error) {
        response.status(500).send(error);
    }
});


/*get alarm */
app.get("/nurse/:id/alarm", async (request, response) => {
    try {

        console.log("Get all alarms");

        var alarms = await AlarmModel.find({$or: [{ nurse_id: request.params.id}, { allNurses: true }]});

        response.send(alarms);

    } catch (error) {
        response.status(500).send(error);
    }
});


var path = require('path');
var dir = path.join(__dirname, 'pictures');
var pictureDefaulfPath = path.join(__dirname, 'default.png');


/*get photo profile */
app.get("/profile/:id", async (request, response) => {
    try {

        console.log("Get profile image");

        var file = path.join(dir, request.params.id + ".png");

        /*if (file.indexOf(dir + path.sep) !== 0) {
            return res.status(403).end('Forbidden');
        }*/
        var type = 'image/png';//mime[path.extname(file).slice(1)] || 'text/plain';
        

        var s = fs.createReadStream(file);

        console.log(pictureDefaulfPath);
        var pictureDefaulfStream = fs.createReadStream(pictureDefaulfPath);

        s.on('open', function () {
            response.set('Content-Type', type);
            s.pipe(response);
        });

        s.on('error', function () {
             console.log("profile picture not found.");
        });

        pictureDefaulfStream.on('open', function () {
            response.set('Content-Type', type);
            pictureDefaulfStream.pipe(response);
        });

        pictureDefaulfStream.on('error', function () {
            response.set('Content-Type', 'text/plain');
            response.status(404).end('Not found');
        });

    } catch (error) {
        response.status(500).send(error);
    }
});


//Uploading new profile pic


app.post('/pictures/:id', upload.single('picture'), (request, response, next) => {
  const file = request.file
  console.log('/pictures');
  console.log(request);
  console.log(request.params.id)
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
    response.send(file)
  
});

//Uploading nurse profile pic


app.post('/nurses/:id/picture', upload.single('picture'), (request, response, next) => {
  const file = request.file


  console.log(request.params.id);
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
    response.send(file)
  
});






