![logo](https://bitbucket.org/giovannedias/avancareapi/raw/dcebc87e38790a8dd7f0bd26ba628211ff89134d/markdown/logo.png)
# Avencare REST API ![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)


This is Centennial group project

This project is based on creating a patient-clinical data management web application for a health care provider(AvenCare). It mainly targets nurses in a hospital or care homes. Allows healthcare providers to manage a group of patients and nurses can create, read and update the health record of every individual patient. With the help of this app, the healthcare providers can find patients in critical condition easily.
## Developers
* ###### Giovanne Emiliano Dias
* ###### Maria Metrina 
* ###### Vishal Patel 
* ###### Dalwinder Singh 


---
## Language
![N|Solid](https://bitbucket.org/giovannedias/avancareapi/raw/dcebc87e38790a8dd7f0bd26ba628211ff89134d/markdown/node.png) 

The application is running with Node JS v10.16.3
```sh
git clone https://giovannedias@bitbucket.org/giovannedias/avancareapi.git
cd avancareapi
npm install
node app.js

```
---
## Backend
![N|Solid](https://bitbucket.org/giovannedias/avancareapi/raw/dcebc87e38790a8dd7f0bd26ba628211ff89134d/markdown/aws.png) ![N|Solid](https://bitbucket.org/giovannedias/avancareapi/raw/dcebc87e38790a8dd7f0bd26ba628211ff89134d/markdown/freebsd.png) 

The application is running on EC2(1) instance whit FreeBSD(2) version 12.

(1) Virtual computing environment, that enables customers to use Web service interfaces to launch instances with a variety of operating systems.
(2) FreeBSD is a popular free and open source operating system that is based on the Berkeley Software Distribution (BSD) version of the Unix operating system.

---
## Database

![N|Solid](https://bitbucket.org/giovannedias/avancareapi/raw/dcebc87e38790a8dd7f0bd26ba628211ff89134d/markdown/mongodb.png)

It was used mongodb version v4.2.0
Model example:
```javascript
const PatientModel = Mongoose.model("patient", {
    first_name: String,
    last_name: String,
    condition: String,
    date_of_birth: String,
    sex: String,
    height: Number,
    weight: Number,
    address: String,
    city: String,
    zip_code: String,
    emergency_contact: String,
    emergency_contact_relationship: String,
    emergency_contact_number:String,
    email: String,
    room: String,
    note: String,
    reports: [{ type: Schema.Types.ObjectId, ref: 'ReportModel'}]

});
```

---
## Tests

![N|Solid](https://bitbucket.org/giovannedias/avancareapi/raw/dcebc87e38790a8dd7f0bd26ba628211ff89134d/markdown/mocha.png)

```javascript
var expect  = require('chai').expect;
var request = require('request');
var fs = require('fs');

var requestPromise = require('request-promise');


describe("Mockup test patient", function() {
     var newPatientIanMcKellenID;
     
     it("mockup Tests Post Request Create New Patient", function(done) {
     
        var newPatientIanMcKellen = { 
            "first_name":"Ian",
            "last_name":"McKellen",
            "date_of_birth":"05/25/1939", 
            "sex" : "M",
            "height" : 180,
            "weight" : 69,
            "address":"75 Sundance Crescent",
            "city" : "Scarborough",
            "zip_code" : "M1G 2M1",
            "emergency_contact" : "Brian Taylor",
            "emergency_contact_relationship" : "Partner",
            "emergency_contact_number" : "416 123 0109",
            "email" : "sir.mckellen@gmail.com",
            "room" : "A101",
            "note" : "Not allergic",
            "condition" : "GOOD"
        };

        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/patients/',
            body: newPatientIanMcKellen,
            json: true, // Automatically stringifies the body to JSON
            resolveWithFullResponse: true
        };

        requestPromise(options)
        .then(function (response,body) {
            console.log("IanMcKellen was created");
            newPatientIanMcKellenID = response.body._id;
            console.log(testPatientID);
            expect(response.statusCode).to.equal(200);
            done();
        })
        .catch(function (err) {
            done();
        });
    });
    
    it("Mockup Picture Profile Upload", function(done) {
        var options = {
            method: 'POST',
            uri: 'http://localhost:3000/pictures/' + newPatientIanMcKellenID,
            formData: {
                picture: {
                        value: fs.createReadStream('./test/ian.png'),
                        options: {
                            filename: 'picture',
                            contentType: 'image/png'
                        }
                }
            },
            resolveWithFullResponse: true
        };

        requestPromise(options)
        .then(function (response,body) {
            console.log("IanMcKellen profile pic created");
            expect(response.statusCode).to.equal(200);
            done();
        })
        .catch(function (err) {
            done();
        });

    });
 });
```


Using chai and mocha for test
```sh
npm test 

```

```npm
npm test test/test-task

```
---
## Documentation

![N|Solid](https://bitbucket.org/giovannedias/avancareapi/raw/dcebc87e38790a8dd7f0bd26ba628211ff89134d/markdown/swagger.png)

Documentation using swagger.
[LINK](http://18.225.10.83:3000/api-docs/) to documentation



